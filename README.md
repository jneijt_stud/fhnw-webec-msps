# webec MSPs inoffizielle Lösungen #

Da für die zur Verfügung stehenden bisherigen MSPs des Moduls Webec keine Lösungen zur Verfügung stehen, habe ich
begonnen diese zu lösen und die Lösungen hier hochzuladen.

**Natürlich werden sich Fehler eingeschlichen haben. Also wenn etwas unklar ist, komisch erscheint oder einfach
falsch ist: bitte melden! ;-)**

Zu jeder MSP hat es noch eine Datei `Tipps.txt` mit Tipps in welche Richtung die Aufgabe geht, oder wonach man suchen 
könnte, falls man nicht weiterkommt beim selbständigen Lösen der Aufgabe.