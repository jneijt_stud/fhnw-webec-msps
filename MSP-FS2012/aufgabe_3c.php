Aufgabe i)
----------
<?php
function activate() {
    create_schema();
}
register_activation_hook(__FILE__, 'activate');
?>

Aufgabe ii)
-----------
<?php
function shortcode($attributes) {
    $atts = shortcode_atts(array('id' => ''), $attributes);
    return process_shortcode($atts['id']);
}
add_shortcode('person', 'shortcode');
?>