/*
Aufgabe i)
----------
*/
function getValue() {
    return document.getElementById('input').value;
}

/*
Aufgabe ii)
-----------
*/
function setContent(text) {
    document.getElementById('output').innerHTML = text;
}

/*
Aufgabe iii)
------------
*/
var chatbot;

function initialise() {
    chatbot = new Chatbot();
}

function process() {
    var text = getValue();
    var answ = chatbot.getAnswer(text);
    setContent(answ);
}