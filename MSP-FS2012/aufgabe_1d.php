Aufgabe i)
----------

<?php
$him = $_GET['him'];
$her = $_GET['her'];
$vote = $_GET['vote'];
?>

Aufgabe ii)
-----------
Bemerkung: Eine Fehlerbehandlung für den Fall, dass das Query nicht erfolgreich ausgeführt wurde, fehlt hier.
Für ein Beispiel dazu, siehe: http://php.net/manual/de/function.mysql-query.php
Da in der Aufgabe nicht von MySQLi die Sprache ist, hier eine Lösung mit den veralteten MySQL-Funktionen und danach eine
mit MySQLi.

In beiden Fällen habe ich mysql_real_escape_string verwendet um die Eingaben von unerwünschten Zeichen zu befreien. Ob
dies in der Aufgabe verlangt ist, weiss ich nicht. Sollte in der Praxis jedenfalls gemacht werden.

<?php
if ($vote == 'yes') {
    $query = "SELECT yes_count
              FROM match
              WHERE (person_a=".mysql_real_escape_string($him)." AND person_b=".mysql_real_escape_string($her).") OR
                    (person_a=".mysql_real_escape_string($her)." AND person_b=".mysql_real_escape_string($him).")";
    $result = mysql_query($query);
    $row = mysql_fetch_assoc($result);
    $previous = $row['yes_count'];

    $query = "UPDATE match
              SET yes_count=".mysql_real_escape_string($previous + 1)."
              WHERE (person_a=".mysql_real_escape_string($him)." AND person_b=".mysql_real_escape_string($her).") OR
                    (person_a=".mysql_real_escape_string($her)." AND person_b=".mysql_real_escape_string($him).")";
    mysql_query($query);
}
?>

Lösung mit MySQLi (unter der Annahme, dass die Datenbankverbindung in der Variable $db gespeichert wurde):

<?php
if ($vote == 'yes') {
    $query = "SELECT yes_count
              FROM match
              WHERE (person_a=".mysql_real_escape_string($him)." AND person_b=".mysql_real_escape_string($her).") OR
                    (person_a=".mysql_real_escape_string($her)." AND person_b=".mysql_real_escape_string($him).")";
    $result = $db->query($query);

    $row = $result->fetch_assoc();
    $previous = $row['yes_count'];

    $query = "UPDATE match
              SET yes_count=".mysql_real_escape_string($previous + 1)."
              WHERE (person_a=".mysql_real_escape_string($him)." AND person_b=".mysql_real_escape_string($her).") OR
                    (person_a=".mysql_real_escape_string($her)." AND person_b=".mysql_real_escape_string($him).")";
    $db->query($query);
}
?>

Aufgabe iii)
------------

<?php
$him = get_random_person('male');
$her = get_random_person('female');

echo '<img src="'.$him.'" style="display: inline;">';
echo '<img src="'.$her.'">';
?>