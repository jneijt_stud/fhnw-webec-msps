/*
Aufgabe i)
----------
Annahme: ID für die neue Person = 3, alle anderen Attribute beliebig
 */
INSERT INTO person (id, username, gender, image) VALUES(3, 'test', 'male', 'profile.png');

/*
Aufgabe ii)
-----------
 */
INSERT INTO video (filename, screenshot, comment, published, person) VALUES('video.mpg', 'video.png', 'Kommentar', 2015-07-03, 3);

/*
Aufgabe iii)
------------
Für Aufgabe 2c zusätzlich hinzugefügt: id, gender
 */
SELECT id, gender, username, image, screenshot, published, comment, filename FROM video AS v
JOIN person AS p ON v.person = p.id