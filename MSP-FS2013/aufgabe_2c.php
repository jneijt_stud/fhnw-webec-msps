<?php
/*
 * Aufgabe i)
 * ----------
 */
class User {
    public $id;
    public $username;
    public $gender;
    public $image;

    public function __construct($id, $username, $gender, $image) {
        $this->id = $id;
        $this->username = $username;
        $this->gender = $gender;
        $this->image = $image;
    }
}

/*
 * Aufgabe ii)
 * -----------
 * Unklarheit: Gemäss Aufgabe sollen alle Attribute aus Aufgabe 2a hinzugefügt werden, plus ein Feld für den Benutzer.
 * Da der Benutzer jedoch bereits in der Datenbank angegeben ist (person), würde man das für das Benutzer-Objekt nutzen.
 * Ob nun zwei oder nur ein Feld gewünscht sind, ist mir unklar.
 */
class Video {
    public $filename;
    public $screenshot;
    public $comment;
    public $published;
    public $person;
    public $user;

    public function __construct($filename, $screenshot, $comment, $published, $person, $user) {
        $this->filename = $filename;
        $this->screenshot = $screenshot;
        $this->comment = $comment;
        $this->published = $published;
        $this->person = $person;
        $this->user = $user;
    }
}

/*
 * Aufgabe iii)
 * ------------
 * Mit "Rezyklierung" von Benutzer Objekten:
 */
function parse($data) {
    $users = array();
    $videos = array();
    foreach($data as $entity) {
        if (!array_key_exists($entity['p.id'], $users)) {
            $users[$entity['p.id']] = new User($entity['id'], $entity['username'], $entity['gender'], $entity['image']);
        }
        $videos[] = new Video($entity['filename'], $entity['screenshot'], $entity['comment'], $entity['published'], $entity['person'], $users[$entity['person']]);
    }
}

/*
 * Ohne:
 */
function parse_simple($data) {
    $videos = array();
    foreach($data as $entity) {
        $user = new User($entity['id'], $entity['username'], $entity['gender'], $entity['image']);
        $videos[] = new Video($entity['filename'], $entity['screenshot'], $entity['comment'], $entity['published'], $entity['person'], $user);
    }
}