<?php
function youtube_shortcode($atts) {
    global $wpdb;
    $query = "SELECT youtube_title FROM plugin_youtube WHERE youtube_id=".$atts['youtube_id'];
    $row = $wpdb->get_row($query, ARRAY_A);
    return '<a href="http://youtu.be/'.$atts['youtube_id'].'">'.$row['youtube_title'].'</a>';
}

add_shortcode('youtube', 'youtube_shortcode');