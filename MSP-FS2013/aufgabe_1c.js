/*
Aufgabe i)
----------
Benötigtes Attribut: id="number"
 */
function getInt() {
    var value = document.getElementById('number').value;
    return parseInt(value, 10);
}

/*
Aufgabe ii)
-----------
Benötigtes Attribut: id="output"
 */
function setInnerHTML(text) {
    var elem = document.getElementById('output');
    elem.innerHTML = rrmerge(elem.innerHTML, text, 3);
}

/*
Aufgabe iii)
------------
 */
function rrmerge(list, text, count) {
    if (list != "") {
        list = list + "<br />" + text;
        list = list.split("<br />");
        while (list.length > count) {
            list.shift();
        }
        return list.join("<br />");
    } else {
        return text;
    }
}