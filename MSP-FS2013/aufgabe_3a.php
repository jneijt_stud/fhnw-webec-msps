<?php
function youtube_activate() {
    global $wpdb;
    $wpdb->query("CREATE TABLE plugin_youtube(
                    youtube_id VARCHAR(10),
                    youtube_title VARCHAR(100)
                  );");
}

register_activation_hook(__FILE__, 'youtube_activate');