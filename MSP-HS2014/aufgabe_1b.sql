/*
Aufgabe i)
----------
Annahmen:
  - id des Benutzers Bill Stinnet = 1
  - id der Aktivität "Skifahren" = 3
 */

INSERT INTO records (user_id, record_time, sensor, activity_id, is_training) VALUES(1, 1421927758, 853234653, 3, 1);

/*
Falls die Aktivität noch nicht existiert, dann zuerst hinzufügen:
 */
INSERT INTO activity (title) VALUES('Skifahren');

/*
Aufgabe ii)
-----------
 */

SELECT AVG(sensor) AS Durchschnitt FROM records
JOIN activity ON records.activity_id=activity.id
WHERE activity.title='Skifahren';