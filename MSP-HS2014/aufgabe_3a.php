<?php
function twitter_shortcode($attributes) {
    $term = $attributes['term'];
    if (isset($attributes['for']) && $attributes['for'] != '') {
        $term = $attributes['for'].$term;
    }
    $connection = new TwitterOAuth();
    $json = $connection->get('search/tweets.json', array('q' => $term, 'count' => 5));
    $res = json_decode($json, true); // das zweite Argument auf true liefert ein Resultat als Array, nicht als Array/Object Gemisch
    $html = '<ul>';
    foreach($res['statuses'] as $status) {
        $html = $html.'<li>'.$status['text'].'</li>';
    }
    $html = $html.'</ul>';
    return $html;
}
?>