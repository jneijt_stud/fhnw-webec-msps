$('input').on('change', function(e) {
    var value = $(this).val();
    if (value >= 12 && value <= 90) {
        $(this).removeClass('red');
        $(this).addClass('green');
    } else {
        $(this).removeClass('green');
        $(this).addClass('red');
    }
});